var express = require('express');
var rethinkrq = require('../modules/rethinkrq');
var router = express.Router();


var flash = require('../modules/flash');
var csv = require('../modules/csv');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/orgs', rethinkrq.orgs);
router.get('/teams', rethinkrq.teams);
router.get('/ops', rethinkrq.operations);
router.get('/req', rethinkrq.requests);
router.get('/his', rethinkrq.history);
router.get('/flash', flash.getJson);
router.get('/up', flash.updateDoc);
router.get('/csv', csv.getCSV);

module.exports = router;
