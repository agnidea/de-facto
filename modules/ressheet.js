/**
 * Created by flower on 18.01.16.
 */
'use strict';
let GoogleSpreadsheet = require("google-spreadsheet");
let creds = require('../keys/soul-way-cbcded68ddd6.json');
let flyd = require('flyd');
let R = require('ramda');
var sugar = require('sugar');


let hasfalse = R.has(false);
module.exports = (sheetID) => {
    let sheetStream = flyd.stream();
    let spreadSheet = new GoogleSpreadsheet(sheetID);
    let allDone = x=>!R.any(R.isEmpty)(Object.values(x));
    spreadSheet.useServiceAccountAuth(creds, err => {
        spreadSheet.getInfo((err, info) => {
            console.log("spreadSheet", info.title);
            let hashRows = {};
            info.worksheets.forEach(sheet=> {
                hashRows[sheet.title] = '';
                sheet.getRows((er, rows)=> {
                    if (R.isEmpty(rows)) rows = true;
                    hashRows[sheet.title] = rows;
                    if (allDone(hashRows)) {
                        console.log("spreadSheet", info.title, "done");
                        sheetStream(hashRows);
                    }
                });
            });
        });
    });
    return sheetStream;
};
