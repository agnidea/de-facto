/**
 * Created by flower on 18.01.16.
 */
'use strict';

let ressheet = require('./ressheet');
let prodsheet = require('./prodsheet');
let flyd = require('flyd');

let Papa = require('babyparse');

let R = require('ramda');
let rethinkrq = require('./rethinkrq');
var moment = require('moment');
moment.locale('ru');

let livesheet = ressheet('1dDITNELreWZ7W2P3y5o54n4cTZck-EUs-LCWOqxy5r0');
let prodSheetOps = prodsheet('16zWnPQfzu77b-CIAgoZY_QfmaKk5O2jDJwhaNr8UFsM');

module.exports.updateDoc = (req, res, next) => {
    flyd.combine((live, prod)=> {
        let l = live();
        let p = prod();
        let mapOps = l["mapOps"];
        let i = 0;
        Object.each(prod(), (opId, opName)=> {
            let sc = mapOps[i];
            console.log(":::", opId, opName);
            sc.prodid = opId;
            sc.prodname = opName;
            sc.save();
            i++;
        });
        res.send("ok")
    }, [livesheet, prodSheetOps]);
}


module.exports.getCSV = (req, res, next) => {
    //console.log("get flash fn()");
    let baseTeams = flyd.stream(rethinkrq.getTable("teams", req._rdbConn));
    //let baseOrgs = flyd.stream(rethinkrq.getTable("organizations", req._rdbConn));
    //let currentTeams = flyd.combine((teams, orgs)=> {
    //    let flexisOrg = R.find(R.propEq('name', "Флексис"))(orgs());
    //    let teamsIndex = R.indexBy(R.prop("id"), teams());
    //    return flexisOrg.teamIds.map(id=>teamsIndex[id]);
    //}, [baseTeams, baseOrgs]);

    console.log("!!!");

    let baseOps = flyd.stream(rethinkrq.getTable("operations", req._rdbConn));
    let baseOrgs = flyd.stream(rethinkrq.getTable("organizations", req._rdbConn));
    let baseReq = flyd.stream(rethinkrq.getTable("requests", req._rdbConn));
    let baseUsers = flyd.stream(rethinkrq.getTable("users", req._rdbConn));
    let baseReqHis = flyd.stream(rethinkrq.getTable("requestStageHistory", req._rdbConn));
    flyd.combine((baseOps, baseReq, baseHis, baseTeams, baseUsers, bOrgs)=> {
        console.log("baseOps, baseReq, baseHis, baseTeams");

        let req = baseReq();
        let orgs = bOrgs();
        let reqIndex = R.indexBy(R.prop("id"), req);
        let his = baseHis();
        let teams = baseTeams();
        let teamsNames = R.indexBy(R.prop("name"), teams);
        let teamsIndex = R.indexBy(R.prop("id"), teams);
        let opsIndex = R.indexBy(R.prop("id"), baseOps());
        let userIndex = R.indexBy(R.prop("id"), baseUsers());


        let out = [];
        let getStage = stageObj => {
            if (stageObj == "toDo") return stageObj
            return Object.keys(stageObj)[0];
        };

        let getUserString = userID =>{
            let user = userIndex[userID];
            if (!user) {
                return "..."
            }
            if (user.profile.lastName) {
                return Object.values(user.profile).join(" ");
            }
            return user.login;
        }

        let getPerfomer = stageObj => {
            let userid = stageObj[getStage(stageObj)].performerId;
            return getUserString(userid);
        }

        //console.log("babyparce", Papa);
        let csv = baseHis().map(h=> {
                let rq = reqIndex[h.requestId];
                let op = opsIndex[rq.operationId];

                return {
                    id: h.requestId,
                    name: op.name,

                    stage: getStage(h.newStage),
                    time: moment(+h.updateTime * 1000).format(),
                    performer: getPerfomer(h.newStage),
                    parents: rq.parentIds,
                    client: getUserString(rq.clientId),
                    opteam: teamsIndex[rq.operationOwnerId].name,

                }
            }
        )
        console.log("send data");
        res.writeHead(200, {
            'Content-Type': 'application/force-download',
            'Content-disposition': 'attachment; filename=involve.csv'
        });

        res.end(Papa.unparse(csv));
        //res.send(Papa.unparse(csv));
    }, [baseOps, baseReq, baseReqHis, baseTeams, baseUsers, baseOrgs]);
};