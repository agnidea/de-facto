/**
 * Created by flower on 18.01.16.
 */
'use strict';

let ressheet = require('./ressheet');
let prodsheet = require('./prodsheet');
let flyd = require('flyd');
let R = require('ramda');
let rethinkrq = require('./rethinkrq');
var moment = require('moment');
moment.locale('ru');

let livesheet = ressheet('1dDITNELreWZ7W2P3y5o54n4cTZck-EUs-LCWOqxy5r0');
let prodSheetOps = prodsheet('1F9PfF8ZPvLd407SjvurPkIXaRD5uK3WrGojHlwWofLs');

module.exports.updateDoc = (req, res, next) => {
    flyd.combine((live, prod)=> {
        let l = live();
        let p = prod();
        let mapOps = l["mapOps"];
        let mapTeams = l["mapTeams"];
        let i = 0;
        console.log("!");

        Object.each(p.ops, (opId, opName)=> {
            let sc = mapOps[i];
            sc.prodname = opName;
            sc.prodid = opId;
            sc.save();
            i++;
        });
        i = 0;
        Object.each(p.teams, (id, value)=> {
            let sc = mapTeams[i];
            sc.prodname = value;
            sc.prodid = id;
            sc.save();
            console.log("id",id,value);

            i++;
        });
        res.send("ok")
    }, [livesheet, prodSheetOps]);
}


module.exports.getJson = (req, res, next) => {
    console.log("get flash fn()");
    let baseTeams = flyd.stream(rethinkrq.getTable("teams", req._rdbConn));
    let baseOrgs = flyd.stream(rethinkrq.getTable("organizations", req._rdbConn));
    let currentTeams = flyd.combine((teams, orgs)=> {
        let flexisOrg = R.find(R.propEq('name', "Флексис"))(orgs());
        let teamsIndex = R.indexBy(R.prop("id"), teams());
        return flexisOrg.teamIds.map(id=>teamsIndex[id]);
    }, [baseTeams, baseOrgs]);

    let baseOps = flyd.stream(rethinkrq.getTable("operations", req._rdbConn));
    let baseReq = flyd.stream(rethinkrq.getTable("requests", req._rdbConn));
    let baseReqHis = flyd.stream(rethinkrq.getTable("requestStageHistory", req._rdbConn));
    flyd.combine((liveS, prod, baseOps, baseReq, baseHis, baseTeams)=> {
        console.log("liveS, prod, baseOps, baseReq, baseHis, baseTeams");

        let live = liveS();
        let req = baseReq();
        let reqIndex = R.indexBy(R.prop("id"), req);
        let his = baseHis();
        let teams = baseTeams();
        //let teamsIndex = R.indexBy(R.prop("id"), teams);
        let teamsNames = R.indexBy(R.prop("name"), teams);
        let opsIndex = R.indexBy(R.prop("id"), baseOps());
        //let opsNames = R.indexBy(R.prop("opname"), baseOps());

        let liveIDToProdName = {};
        let liveIDToProdID = {};
        let prodIDtoName = {};
        let connect = row => {
            prodIDtoName[row.prodid] = row.prodname;
            liveIDToProdID[row.connectid] = row.prodid;
            liveIDToProdName[row.connectid] = row.prodname;
        };

        live["mapTeams"].forEach(connect);
        live["mapOps"].forEach(connect);


        let getBaseByLiveId = liveID => {
            let prodID = liveIDToProdID[liveID];
            if (!prodID) return false;
            let getTeamByProdId = (teamProdId)=> {
                console.log("te",teamProdId,prodIDtoName[teamProdId]);

                let teamName = prodIDtoName[teamProdId];
                return teamsNames[teamName];
            };
            let getBase = R.cond([
                [id=>id.indexOf(".") > 0, id=> {
                    let teamProdId = id.split(".")[0];
                    let team = getTeamByProdId(teamProdId);
                    if (!team.operationIds){
                        console.log("11");
                    }
                    let teamOps = team.operationIds.map(o=>opsIndex[o]);
                    let opName = prodIDtoName[id];
                    let operation = R.find(R.propEq('name', opName))(teamOps);
                    if (!operation){
                        console.log("!!!");
                    }
                    operation.isOperation = true;
                    return operation;
                }],
                [R.T, getTeamByProdId]
            ]);
            return getBase(prodID);
        }

        //let baseToLiveID = R.invertObj(liveIDToProdName);


        let getStage = stageObj => {
            if (stageObj == "toDo") return stageObj
            return Object.keys(stageObj)[0];
        }

        let cards = live["Cards"];
        let haveMedians = {};
        cards.forEach(row=> {
                let baseObj = getBaseByLiveId(row.id);
                if (baseObj && baseObj.isOperation) {
                    //console.log(baseObj.name);

                    if (baseObj.leadTimeHistogram) {
                        let median = baseObj.leadTimeHistogram.median;
                        if (median) {
                            let duration = moment.duration((+median * 1000), "ms").humanize();
                            haveMedians[row.id] = duration;
                            row.median = duration;
                        }
                    }
                    let allrequests = R.filter(R.propEq('operationId', baseObj.id))(req);

                    row.trafic = allrequests.length;
                    row.save();
                    //console.log("trafic", row.trafic);
                }
                else {
                    //console.log("row", row.id);

                }
            }
        );

        live["Connections"].forEach(row=> {
            let opFrom = getBaseByLiveId(row.from);
            let opTo = getBaseByLiveId(row.to);
            let toReq = R.filter(R.propEq('operationId', opTo.id))(req);
            //console.log("toReq",toReq);
            let medianR = -1;
            let allOpRqDurations = [];
            if (toReq.length > 0 && opFrom) {
                toReq.forEach(r=> {
                    if (r.parentIds.length) {
                        r.parentIds.forEach(p=> {
                            let pr = reqIndex[p];
                            if (pr.operationId == opFrom.id) {
                                let rh = R.filter(R.propEq('requestId', p))(his);
                                let start = R.find(R.propEq('newStage', 'toDo'), rh);
                                let rqDuration = (+pr.updateTime) - (+start.updateTime);
                                allOpRqDurations.push(Math.round(rqDuration * 1000));
                                console.log("r:", opTo.name, getStage(pr.stage), pr.updateTime, start.updateTime, rqDuration);
                            }
                        })
                    }
                })
            }

            if (allOpRqDurations.length > 0)
                medianR = R.median(allOpRqDurations);
            if (medianR > 0) {
                let duration = moment.duration((medianR), "ms").humanize();
                console.log("duration", duration);
                row.time = `.. ${duration}`;
                row.save();
            }

            //let connectionKeys = R.keys(live["Connections"][0])


            //next();
        })

        res.send({
            'Cards': live["Cards"].map(x=>R.pickAll(
                ["label", "id", 'x', 'y', 'h', 'w', 'type', 'expand', 'fontsize', 'trafic','median']
                , x)),
            'Connections': live["Connections"].map(x=>R.pickAll(
                ['from','to','x1','y1','x2','y2','x3','y3','x4','y4','x5','y5','x6','y6','time','color']
                ,x))
        });
        console.log("send data");
        //res.send('xx');
    }, [livesheet, prodSheetOps, baseOps, baseReq, baseReqHis, currentTeams]);
};