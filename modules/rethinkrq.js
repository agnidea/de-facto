'use strict';
let r = require('rethinkdb');
let con = require('../connections');
//
let getTable = module.exports.getTable = (tableName,rdbcon)=>
    r.db("involve").table(tableName)
    .run(rdbcon)
    .then(cursor=>cursor.toArray());

module.exports.orgs = (req, res, next)=> {
    getOrganizations("organizations", req._rdbConn)
        .then(result => res.send(result))
        .error(cc.handleError(res))
};
console.log("cc",con.handleError);


module.exports.teams = (req, res, next) => {
    r.db("involve").table("teams").run(req._rdbConn).then(function (cursor) {
        return cursor.toArray();
    }).then(function (result) {
        res.send(result);
    }).error(con.handleError(res))
};

module.exports.operations = (req, res, next) => {
    r.db("involve").table("operations").run(req._rdbConn).then(function (cursor) {
        return cursor.toArray();
    }).then(function (result) {
        res.send(result);
    }).error(con.handleError(res))
};

module.exports.requests = (req, res, next) => {
    r.db("involve").table("requests").run(req._rdbConn).then(function (cursor) {
        return cursor.toArray();
    }).then(function (result) {
        res.send(result);
    }).error(con.handleError(res))
};

module.exports.history = (req, res, next) => {
    r.db("involve").table("requestsHistory").run(req._rdbConn).then(function (cursor) {
        return cursor.toArray();
    }).then(function (result) {
         res.send(result);
    }).error(con.handleError(res))
};

