/**
 * Created by flower on 18.01.16.
 */
'use strict';
let GoogleSpreadsheet = require("google-spreadsheet");
let creds = require('../keys/soul-way-cbcded68ddd6.json');
let flyd = require('flyd');
let R = require('ramda');
var sugar = require('sugar');


module.exports = (sheetID) => {
    let prodStream = flyd.stream();
    let spreadSheet = new GoogleSpreadsheet(sheetID);
    let allDone = x=>!R.any(R.isEmpty)(Object.values(x));
    let hashCells = {};
    spreadSheet.useServiceAccountAuth(creds, function (err) {
        spreadSheet.getInfo(function (err, info) {
            console.log("spreadSheet", info.title);
            info.worksheets.forEach(sheet=> {
                hashCells[sheet.title] = '';
                sheet.getCells((er, cells)=> {
                    hashCells[sheet.title] = cells;
                    if (allDone(hashCells)) {
                        prodStream( {
                            "ops": parseOps(),
                            "teams": parseTeams()
                        })
                    }
                });
            });
        });

    });
    let parseTeams = ()=> {
        let teams = {};
        Object.each(hashCells, (teamID, cells) => {
            teamID = teamID.replace("Team.", "");
            teams[teamID] = cells[1].value;
        })
        return teams;
    }
    let parseOps = ()=> {
        let opsIDs = {};
        let onlyNeeded = x=>x.col >= 3 && x.row >= 7 && x.col <= 4;
        Object.each(hashCells, (teamID, cells) => {
            teamID = teamID.replace("Team.", "") + ".";
            let rows = {};
            cells.forEach(c=> {
                if (!onlyNeeded(c)) return;
                if (!rows[c.row]) rows[c.row] = {};
                rows[c.row][c.col] = c.value;
                //console.log(":::", rows[c.row][c.col]);
            })
            Object.each(rows, (k, v)=> {
                opsIDs[teamID + v['3']] = v['4'];
            });
        })
        return opsIDs;
    }
    return prodStream;
};
