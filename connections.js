'use strict';
var r = require('rethinkdb');
var config = {
    rethinkdb: {
        host: 'localhost',
        port: 28015,
        authKey: '',
        db: 'rethinkdb_ex'
    }
}



function handleError(res) {
    return function (error) {
        console.log("error", error.message);
        res.status(500).send({error: error.message});
    }
}


class Connections {
    constructor() {
        r.connect(config.rethinkdb, function (err, conn) {
            if (err) {
                console.log("Could not open a connection to initialize the database");
                console.log(err.message);
                process.exit(1);
            }
        })
    }

    handleError(res) {
        return handleError
    }

    createConnection(req, res, next) {

        console.log("createConnection set setHeaders");
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        res.setHeader('Access-Control-Allow-Credentials', true);
        r.connect(config.rethinkdb).then(function (conn) {
            req._rdbConn = conn;
            next();
        })
            .error(handleError(res));
    }

    closeConnection(req, res, next) {
        req._rdbConn.close();
        console.log("closeConnection\n")
    }


}

let con = new Connections();

module.exports = con;

